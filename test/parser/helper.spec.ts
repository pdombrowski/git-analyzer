import * as helper from '../../src/parser/helper';
import { expect } from 'chai'

describe('isHeader helper works correctly', () => {
    it("return true when header start with correct tag", () => {
        expect(true).is.equal(helper.isHeader('{{--header--}}{{-name}}Roland Franssen{{--name}}{{-date}}2019-04-02T12:04:04+02:00{{--date}}{{-hash}}c3477badbcb517df3be95d6294a9d740dd0d7cac{{--hash}}{{-message}}[EventDispatcher] Split events across requests{{--message}}'))
    })
    it("return false when header doesent start with correct tag", () => {
        expect(false).is.equal(helper.isHeader('{--header--}}{{-name}}Roland Franssen{{--name}}{{-date}}2019-04-02T12:04:04+02:00{{--date}}{{-hash}}c3477badbcb517df3be95d6294a9d740dd0d7cac{{--hash}}{{-message}}[EventDispatcher] Split events across requests{{--message}}'))
    })
});

describe('parseHeader helper works correctly', () => {
    const headerLine = '{{--header--}}{{--name--}}Roland Franssen{{--name--}}{{--date--}}2019-04-02T12:04:04+02:00{{--date--}}{{--hash--}}c3477badbcb517df3be95d6294a9d740dd0d7cac{{--hash--}}{{--message--}}[EventDispatcher] Split events across requests{{--message--}}';
    const result = helper.parseHeader(headerLine);
    it("name is Roland Franssen", () => {
        expect('Roland Franssen').is.equal(result.contributor);
    });
    it("date is Correct", () => {
        expect(new Date('2019-04-02T12:04:04+02:00').getTime()).is.equal(result.date.getTime());
    })
    it("hash is c3477badbcb517df3be95d6294a9d740dd0d7cac", () => {
        expect('c3477badbcb517df3be95d6294a9d740dd0d7cac').is.equal(result.hash);
    })
    it("message is [EventDispatcher] Split events across requests", () => {
        expect('[EventDispatcher] Split events across requests').is.equal(result.message);
    })    
});

describe('parseLine helper works correctly with moved file', () => {
    const line = '21      19      src/Symfony/Bridge/Twig/Mime/{Renderer.php => BodyRenderer.php}';
    const result = helper.parseChangeFileLine(line);
    it("added is 21", () => {
        expect(21).is.equal(result.added);
    });
    it("removed is 19", () => {
        expect(19).is.equal(result.removed);
    })
    it("isMoved is true", () => {
        expect(true).is.equal(result.isMoved);
    })
    it("pathname is src/Symfony/Bridge/Twig/Mime/BodyRenderer.php", () => {
        expect('src/Symfony/Bridge/Twig/Mime/BodyRenderer.php').is.equal(result.filepath);
    })
    it("oldpathname is src/Symfony/Bridge/Twig/Mime/Renderer.php", () => {
        expect('src/Symfony/Bridge/Twig/Mime/Renderer.php').is.equal(result.oldFilePath);
    })
    it("extension is php", () => {
        expect('php').is.equal(result.extension);
    }) 
});

describe('parseLine helper works correctly with moved file without braces', () => {
    const line = '48	42	Readme => Readme.md';
    const result = helper.parseChangeFileLine(line);
    it("added is 48", () => {
        expect(48).is.equal(result.added);
    });
    it("removed is 42", () => {
        expect(42).is.equal(result.removed);
    })
    it("isMoved is true", () => {
        expect(true).is.equal(result.isMoved);
    })
    it("pathname is Readme.md", () => {
        expect('Readme.md').is.equal(result.filepath);
    })
    it("oldpathname is Readme", () => {
        expect('Readme').is.equal(result.oldFilePath);
    })
    it("extension is md", () => {
        expect('md').is.equal(result.extension);
    }) 
});


describe('parseLine helper works correctly without moved file', () => {
    const line = '21      19      src/Symfony/Bridge/Twig/Mime/Renderer.php';
    const result = helper.parseChangeFileLine(line);
    
    it("isMoved is false", () => {
        expect(false).is.equal(result.isMoved);
    })
    it("pathname is src/Symfony/Bridge/Twig/Mime/Renderer.php", () => {
        expect('src/Symfony/Bridge/Twig/Mime/Renderer.php').is.equal(result.filepath);
    })
    it("oldpathname is src/Symfony/Bridge/Twig/Mime/Renderer.php", () => {
        expect(null).is.equal(result.oldFilePath);
    })
    it("extension is php", () => {
        expect('php').is.equal(result.extension);
    }) 
});

describe('parseLine helper works correctly with no added and no removed', () => {
    const line = '-      -      src/Symfony/Bridge/Twig/Mime/Renderer.php';
    const result = helper.parseChangeFileLine(line);
    it("added is 0", () => {
        expect(0).is.equal(result.added);
    });
    it("removed is 0", () => {
        expect(0).is.equal(result.removed);
    })
});