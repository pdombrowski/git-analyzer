import {IReporter, TReport} from "./IReporter";
import {TChanges, TFileChanges} from "../parser";

type TTags = { [key: string]: number };
type TContributor = { name: string, added: number, removed: number, commits: number };
type TFile = { path: string, contributors: Array<string>, hits: number, tags: TTags }
type TSummary = {
    commits: number,
    added: number,
    removed: number,
    tags: TTags
};

export class MemoryReporter implements IReporter {
    private contributors: { [key: string]: TContributor; } = {};
    private files: { [key: string]: TFile } = {};
    private summary: TSummary = {
        commits: 0,
        added: 0,
        removed: 0,
        tags: {}
    }

    private exclude = [
        "History.md",
        "package.json",
        "test/",
        "docs/"
    ]

    constructor(changes: Array<TChanges>, config: object) {
        changes.forEach((change: TChanges) => {
            this.parseChanges(change)
        })
    }

    public report(): TReport {
        const contributors: Array<TContributor> = Object.values(this.contributors);
        const files: Array<TFile> = Object.values(this.files);
        return {
            summary: JSON.parse(JSON.stringify(this.summary)),
            files: files,
            contributors: contributors
        }
    }

    private parseChanges(change: TChanges) {
        let allowedChanges = change.changes.filter((singleChange) => {
            return this.filepathIsAllowed(singleChange.filepath)
        })
        if (allowedChanges.length === 0) {
            return;
        }
        let changesSummary = allowedChanges.reduce((summary, change) => {
            summary.added += change.added;
            summary.removed += change.removed;
            return summary;

        }, {added: 0, removed: 0});

        this.summary.added += changesSummary.added;
        this.summary.removed += changesSummary.removed;
        this.summary.commits++;
        this.parseContributor(change.commit.contributor, changesSummary.added, changesSummary.removed);
        const tags = this.tagSearch(change);
        this.summary.tags = this.tagCount(tags, this.summary.tags);
        allowedChanges.forEach((singleChange) => {
            this.parseFileChange(singleChange, change.commit.contributor, tags)
        })
    }

    private tagSearch(change: TChanges): Array<string> {
        const commitMessage: string = change.commit.message;
        if (commitMessage.includes('bug')) {
        }

        const hasBugWord = commitMessage.search(/\bbug\b/i) >= 0 ? 'bug' : null;
        const hasBugfixWord = commitMessage.search(/\bbugfix\b/i) >= 0 ? 'bugfix' : null;
        const hasFixesWord = commitMessage.search(/\bfixes\b/i) >= 0 ? 'fixes' : null;
        const hasFixedWord = commitMessage.search(/\bfixes\b/i) >= 0 ? 'fixed' : null;
        const isProbablyFix = hasBugfixWord || hasBugWord || hasFixesWord || hasFixedWord ? 'isFix' : null;

        return [hasBugfixWord, hasFixedWord, hasBugWord, hasFixesWord, isProbablyFix].filter((tag => tag !== null));
        return [isProbablyFix].filter((tag) => {
            return tag !== null
        });
    }

    private tagCount(tagList: Array<string>, tags: TTags): TTags {
        tagList.forEach((tag) => {
            if (tags.hasOwnProperty(tag)) {
                tags[tag]++
            } else {
                tags[tag] = 1
            }
        })
        return tags;
    }

    private parseContributor(contributor: string, added: number, removed: number): void {
        if (!this.contributors.hasOwnProperty(contributor)) {
            this.contributors[contributor] = {
                name: contributor,
                commits: 1,
                added: added,
                removed: removed
            }
        } else {
            this.contributors[contributor].commits++;
            this.contributors[contributor].added += added;
            this.contributors[contributor].removed += removed;
        }
    }

    private parseFileChange(fileChange: TFileChanges, contributor: string, tags: Array<string>) {
        if (!this.files.hasOwnProperty(fileChange.filepath)) {
            this.files[fileChange.filepath] = {
                contributors: [contributor],
                path: fileChange.filepath,
                hits: 1,
                tags: this.tagCount(tags, {})
            }
        } else {
            const filePathChange = this.files[fileChange.filepath];
            if (filePathChange.contributors.indexOf(contributor) === -1) {
                filePathChange.contributors.push(contributor)
            }
            filePathChange.hits++;
            filePathChange.tags = this.tagCount(tags, filePathChange.tags);
        }
    }

    private filepathIsAllowed(filepath: string) {
        const exclude = this.exclude;
        for (let i = 0; i < exclude.length; i++) {
            const disallowedByPattern = (exclude[i].endsWith('/') || exclude[i].endsWith('*')) && filepath.startsWith(exclude[i])
            const exactlyDisallowed = filepath === exclude[i];
            if (disallowedByPattern || exactlyDisallowed) {
                return false;
            }
        }
        return true;
    }

}