export interface IReporter {
    report(): TReport
}

export type TReport = {
    summary: TSummary,
    files: Array<TFile>,
    contributors: Array<TContributor>
}

type TContributor = {
    name: string,
    added: number,
    removed: number,
    commits: number
}

type TSummary = {
    commits: number,
    added: number,
    removed: number,
    bugFixes: number,
    tags: { [key: string]: number }
}

type TFile = {
    path: string,
    contributors: Array<string>,
    hits: number,
    tags: { [key: string]: number }
}

interface IReport {
    getSummary(): TSummary,

    getFiles: Array<TFile>,
    getContributors: Array<TContributor>,
}

enum Order {asc = 'asc', desc = 'desc'}

interface IResultCollection {
    sortByKey(key: string, sort?: Order): IResultCollection,

    sort(cb: Function): IResultCollection,

    getValues(): Array<any>
}