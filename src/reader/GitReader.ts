import * as childProcess from 'child_process';
import * as util from 'util';

const exec = util.promisify(childProcess.exec);

export class GitReader {
    private pattern: string = '{{--header--}}{{--name--}}%aN{{--name--}}{{--date--}}%cI{{--date--}}{{--hash--}}%H{{--hash--}}{{--message--}}%s{{--message--}}';
    private cmd: string = `git log --numstat --no-merges --oneline --pretty=format:${this.pattern}`;

    public async read(gitpath: string): Promise<string> {
        try {
            const result = await exec(`cd ${gitpath} && ${this.cmd}`, {maxBuffer: 1024 * 1024 * 200});
            return result.stdout;
        } catch (error) {
            console.log(error.stderr)
            return '';
        }
    }
}