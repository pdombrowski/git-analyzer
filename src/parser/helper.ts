import * as path from 'path';
import {TCommit, TFileChanges} from './types'

type HeaderTags = {
    contributor: string,
    date: string,
    hash: string,
    message: string
}

const defaultHeaderTags: HeaderTags = {
    contributor: '{{--name--}}',
    date: '{{--date--}}',
    hash: '{{--hash--}}',
    message: '{{--message--}}'
}

function isHeader(line: string): boolean {
    return line.indexOf('{{--header--}}') === 0;
}

function isLine(line: string): boolean {
    return /(\d+|\-)\s+(\d+|\-)\s+.*/.test(line);
}

function parseHeader(headerString: string): TCommit {
    if (!isHeader(headerString)) {
        throw new Error(`${headerString} line is not a header`);
    }

    const header = {
        contributor: extractValueFromHeaderByTag(defaultHeaderTags.contributor, headerString),
        date: new Date(extractValueFromHeaderByTag(defaultHeaderTags.date, headerString)),
        hash: extractValueFromHeaderByTag(defaultHeaderTags.hash, headerString),
        message: extractValueFromHeaderByTag(defaultHeaderTags.message, headerString)
    };

    return header;
}

function parseChangeFileLine(lineString: string): TFileChanges {
    if (!isLine(lineString)) {
        throw new Error(`${lineString} is not a line string`);
    }
    const addedOrRemoved = lineString.match(/(\d+|\-)/g);
    const added = parseInt(addedOrRemoved[0], 10) || 0;
    const removed = parseInt(addedOrRemoved[1], 10) || 0;
    const guessedPath = lineString.replace(/(\d+|\-)\s+(\d+|\-)\s+/, '')
    const isMoved = guessedPath.indexOf(' => ') >= 0;

    const filePath = isMoved ? getMovedFilePaths(guessedPath).new : guessedPath
    const oldFilePath = isMoved ? getMovedFilePaths(guessedPath).old : null;
    const extension = path.extname(filePath).replace('.', '');
    return {
        added: added,
        removed: removed,
        filepath: filePath,
        extension: extension,
        oldFilePath: oldFilePath,
        isMoved: isMoved
    }
}

function getMovedFilePathsWithBracket(filePath: string) {
    const regexp = /\{.*\}/;
    const paths = filePath.match(regexp)[0].replace(/[\{\}]/g, '').split(' => ');
    return {
        old: filePath.replace(regexp, paths[0]),
        new: filePath.replace(regexp, paths[1]),
    }
}

function getMovedFilePathsWithoutBracket(filepath: string) {
    const paths = filepath.split(' => ');
    return {
        old: paths[0],
        new: paths[1]
    }
}

function getMovedFilePaths(filePath: string) {
    return filePath.indexOf('{') > -1 ? getMovedFilePathsWithBracket(filePath) : getMovedFilePathsWithoutBracket(filePath);
}

function extractValueFromHeaderByTag(tag: string, text: string): string {
    try {
        const result = text.match(new RegExp(`${tag}([\\s\\S]*)${tag}`));
        return result[1];
    } catch (e) {
        console.error(tag, text, '--regexp--', `${tag}([\\s\\S]*)${tag}`)
        throw e;
    }

}

export {
    isHeader, parseHeader, parseChangeFileLine
}