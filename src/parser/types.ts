export type TCommit = {
    contributor: string,
    date: Date,
    hash: string,
    message: string
}

export type TFileChanges = {
    added: number,
    removed: number,
    filepath: string,
    extension: string,
    oldFilePath: string | null,
    isMoved: boolean
}

export type TChanges = {
    commit: TCommit,
    changes: Array<TFileChanges>
}