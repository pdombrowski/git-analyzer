import * as Helper from "./helper"
import {TChanges} from './types'

export class GitParser {
    public parse(gitlog: string): Array<TChanges> {
        let currentCommit: TChanges = null;
        const changes: Array<TChanges> = []
        const gitLines = gitlog.split('\n');
        gitLines.forEach((line) => {
            if (Helper.isHeader(line)) {
                if (currentCommit !== null) {
                    changes.push(currentCommit);
                }
                currentCommit = {
                    commit: Helper.parseHeader(line),
                    changes: []
                }
            } else {
                if (line === '') {
                    return;
                }
                const fileChanges = Helper.parseChangeFileLine(line);
                currentCommit.changes.push(fileChanges)
            }
        })
        return changes;
    }
}