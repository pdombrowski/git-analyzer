import {GitReader} from './reader/GitReader';
import {GitParser} from './parser/GitParser';
import {MemoryReporter} from "./reporter/MemoryReporter";
import {TReport} from "./reporter/IReporter";
import {Null} from "./output/Null";
import {Blessed} from "./output/Blessed";
import ora from 'ora';
import fs from 'fs';

// const config = JSON.parse(fs.readFileSync(__dirname + '/../config/config.json', 'utf8'));
const config = {};

const gitReader = new GitReader();
const gitParser = new GitParser();

export class App {
    private async getData(gitpath: string): Promise<string> {
        return await gitReader.read(gitpath);
    }

    public async analyze(gitpath: string) {
        const spinner = ora({
            text: 'Collecting git log'
        }).start();
        const result = await this.getData(gitpath);
        spinner.succeed('Collected git log')
        if (!result) {
            throw new Error('cannot get git log')
        }
        spinner.start('Parsing git log')
        const parsed = gitParser.parse(result);
        spinner.succeed('Parsed git log')
        spinner.start('Analyzing data')
        const memoryReporter = new MemoryReporter(parsed, config);
        spinner.succeed('Analyzed data')
        const report: TReport = memoryReporter.report();
        const output = new Blessed();
        // const output = new Null();
        output.print(report);
    }
};