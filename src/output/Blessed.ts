import {IOutput} from "./IOutput";
import {TReport} from "../reporter/IReporter";

import blessed from 'blessed';
import contrib from 'blessed-contrib';

type TTagStats = { [key: string]: Array<any> }

export class Blessed implements IOutput {
    private screen: any;
    private grid: any;
    private tagCountStats: TTagStats;
    private tagCountPercentStats: TTagStats;
    private tags: Array<string>;
    private currentTag: string;
    private tagStatistics = ['tagCountStats', 'tagCountPercentStats'];
    private filesByTagsCountTable: any;

    public constructor() {
        this.screen = blessed.screen();

        this.grid = new contrib.grid({rows: 3, cols: 6, screen: this.screen});
        this.screen.key(['escape', 'q', 'C-c'], () => {
            return process.exit(0)
        });
        this.screen.key([']'], () => {
            this.goToNextTag()
        });
        this.screen.key(['['], () => {
            this.goToPrevTag()
        });

        this.screen.key(['l'], () => {
            this.toggleTagStats()
        });
    }

    public print(report: TReport): void {
        this.tags = Object.keys(report.summary.tags)
        if (this.tags.length > 0) {
            this.currentTag = this.tags[0]
        }

        this.prepareFilesByTagsCount(report)
        this.prepareFilesByTagsPercent(report)
        this.printContributorsByChanges(report)
        this.printContributorsByCommits(report)
        this.printFilesByHits(report);
        this.printFilesByContributors(report);
        this.prepareFilesByTagsCount(report);
        this.screen.render();
    }

    private toggleTagStats() {
        this.tagStatistics.push(this.tagStatistics.shift());
        this.setFilesByTagsCount();
        this.screen.render()
    }

    private createFilesByTagsCount() {
        const label = `files by tags`;

        return this.grid.set(2, 0, 1, 3, contrib.table, {
            label: label,
            keys: false,
            interactive: false,
            columnWidth: [45, 8, 10, 15],
        })
    }

    private getCurrentStatistict(): string {
        return this.tagStatistics[0];
    }

    private setFilesByTagsCount() {
        const currentTag = this.getCurrentTag();
        const currentStatistics = this.getCurrentStatistict()
        //@ts-ignore
        const data = this[currentStatistics].hasOwnProperty(currentTag) ? this[currentStatistics][currentTag] : []
        const headers = ['filepath', 'commiters', 'tagCount', 'tag/commits[%]']
        const label = `files by tags. ${this.getTagsLabels()}`
        this.filesByTagsCountTable.setLabel(label)
        this.filesByTagsCountTable.setData({
            headers: headers,
            data: data
        })
    }

    private getCurrentTag(): string {
        return this.tags.length >= 3 ? this.tags[1] : this.tags[0];
    }

    private getTagsLabels() {
        const tags = this.tags.slice(0, 3);
        if (tags.length >= 3) {
            tags[1] = `[${tags[1]}]`
        } else {
            tags[0] = `[${tags[0]}]`
        }
        const tagsText = tags.join(' ');
        return `currentTag: ${this.getCurrentTag()}. currentStatistic: ${this.getCurrentStatistict()}`
    }

    private goToNextTag(): void {
        this.tags.push(this.tags.shift());
        this.setFilesByTagsCount();
        this.screen.render()
    }

    private goToPrevTag(): void {
        this.tags.unshift(this.tags.pop())
        this.setFilesByTagsCount();
        this.screen.render()
    }

    private printFilesByContributors(report: TReport) {
        const filesByHitsTable = this.grid.set(1, 3, 1, 3, contrib.table, {
            label: 'files by commiters',
            keys: false,
            interactive: false,
            columnWidth: [50, 5, 10],
        })

        const files = report.files;
        files.sort((file1, file2) => {
            return file2.contributors.length - file1.contributors.length
        })
        const data = files.slice(0, 15).map((file) => {
            return [this.cutFilePath(file.path, 45), file.hits, file.contributors.length]
        })
        filesByHitsTable.setData({
            headers: ['path', 'hits', 'commiters'],
            data: data
        })
    }

    private prepareFilesByTagsCount(report: TReport): void {
        const tags = Object.keys(report.summary.tags);
        const statsByTag: TTagStats = {};
        const files = report.files;
        tags.forEach((tag) => {
            files.sort(function (file1, file2) {
                var file1TagCount = file1.tags.hasOwnProperty(tag) ? file1.tags[tag] : 0
                var file2TagCount = file2.tags.hasOwnProperty(tag) ? file2.tags[tag] : 0
                return file2TagCount - file1TagCount;
            })

            statsByTag[tag] = files.slice(0, 15).map((file) => {
                return [
                    this.cutFilePath(file.path, 45),
                    file.contributors.length,
                    file.tags[tag],
                    `${this.round(file.tags[tag] / file.hits, 2, true)}%`
                ]
            });
        })
        this.tagCountStats = statsByTag;
        this.filesByTagsCountTable = this.createFilesByTagsCount();
        this.setFilesByTagsCount();
    }

    private prepareFilesByTagsPercent(report: TReport): void {
        const tags = Object.keys(report.summary.tags);
        const statsByTag: TTagStats = {};
        const files = report.files;
        tags.forEach((tag) => {

            files.sort(function (file1, file2) {
                var file1TagCount = file1.tags.hasOwnProperty(tag) ? file1.tags[tag] / file1.hits : 0
                var file2TagCount = file2.tags.hasOwnProperty(tag) ? file2.tags[tag] / file2.hits : 0
                return file2TagCount - file1TagCount;
            })

            statsByTag[tag] = files.slice(0, 15).map((file) => {
                return [
                    this.cutFilePath(file.path, 45),
                    file.contributors.length,
                    file.tags[tag],
                    `${this.round(file.tags[tag] / file.hits, 2, true)}%`
                ]
            });
        })
        this.tagCountPercentStats = statsByTag;
    }

    private printFilesByHits(report: TReport) {
        const filesByHitsTable = this.grid.set(1, 0, 1, 3, contrib.table, {
            label: 'files by commits',
            keys: false,
            interactive: false,
            columnWidth: [50, 10, 5],
        })

        const files = report.files;
        this.sortByKey(files, 'hits', 'desc')
        const data = files.slice(0, 15).map((file) => {
            return [this.cutFilePath(file.path, 45), file.contributors.length, file.hits]
        })
        filesByHitsTable.setData({
            headers: ['path', 'commiters', 'hits'],
            data: data
        })

    }

    private printContributorsByChanges(report: TReport) {
        const contributorsTable = this.grid.set(0, 2, 1, 4, contrib.table, {
            label: 'contributors by changes',
            keys: false,
            interactive: false,
            columnWidth: [35, 8, 8, 8, 8, 8],
        })


        const contributors = report.contributors;
        contributors.sort((item1, item2) => {
            return (item2.added + item2.removed) - (item1.added + item1.removed)
        })

        const data = contributors.slice(0, 10).map((contributor) => {
            const contributorChanges = contributor.added + contributor.removed;
            const summaryChanges = report.summary.added + report.summary.removed;
            const percentCommits = this.round(contributorChanges / summaryChanges, 2, true)
            const percenteChangesText = `${percentCommits}%`
            const contributorName = contributor.name.length > 25 ? contributor.name.substring(0, 22) + '...' : contributor.name
            return [
                contributorName,
                this.toScientificNotation(contributor.commits),
                this.toScientificNotation(contributor.added),
                this.toScientificNotation(contributor.removed),

                this.toScientificNotation(contributorChanges),
                percenteChangesText]
        })
        contributorsTable.setData({
            headers: ['commiter', 'commits', 'added', 'removed', 'changes', 'changes %'],
            data: data
        })
    }

    private printContributorsByCommits(report: TReport) {
        const contributorsTable = this.grid.set(0, 0, 1, 2, contrib.table, {
            label: 'contributors by commits',
            keys: false,
            interactive: false,
            columnWidth: [25, 10, 10],
        })

        const contributors = report.contributors;
        this.sortByKey(contributors, 'commits', 'desc')
        const data = contributors.slice(0, 10).map((contributor) => {
            const percentCommits = this.round(contributor.commits / report.summary.commits, 2, true)
            const percenteCommitsText = `${percentCommits}%`
            const contributorName = contributor.name.length > 25 ? contributor.name.substring(0, 22) + '...' : contributor.name
            return [contributorName, contributor.commits.toString(), percenteCommitsText]
        })
        contributorsTable.setData({
            headers: ['commiter', 'commits', 'participation'],
            data: data
        })
    }

    private toScientificNotation(number: number): string {
        switch (true) {
            case number > 10000:
                return `${this.round(number / 1000, 0)}k`
            case number > 10000000:
                return `${this.round(number / 1000000, 0)}M`
            default:
                return number.toString()
        }
    }

    private round(value: number, precision: number, percent?: boolean): number {
        if (percent === true) {
            value = value * 100
        }
        const multipler = Math.pow(10, precision);
        return Math.round(value * multipler) / multipler;
    }

    private sortByKey(items: Array<any>, key: string, order: string) {
        items.sort((item1, item2) => {
            if (order.toLowerCase() === 'desc') {
                return item2[key] - item1[key]
            }
            return item1[key] - item2[key]
        })
    }

    private cutFilePath(filePath: string, maxLength: number) {
        if (filePath.length <= maxLength) {
            return filePath;
        }
        const separator = '...';
        let lengthRemaining = maxLength - separator.length - 2;

        const explodedPath = filePath.split('/');
        const first = explodedPath[0];
        const last = explodedPath.pop();

        if (first.length + last.length > lengthRemaining) {
            return last.substring(0, lengthRemaining) + '...'
        }

        lengthRemaining -= (first.length + last.length)
        const endPath = [last];

        while (lengthRemaining > 0) {
            const dir = explodedPath.pop()
            lengthRemaining -= dir.length;
            lengthRemaining--;
            endPath.unshift(dir);
        }
        endPath.shift();
        endPath.unshift('...')
        endPath.unshift(first);
        return endPath.join('/');
    }
}