import {TReport} from "../reporter/IReporter";

export interface IOutput {
    print(report: TReport): void
}