import {IOutput} from "./IOutput";
import {TReport} from "../reporter/IReporter";

export class Null implements IOutput {
    public print(report: TReport): void {

    }
}