import {App} from './App'

const app = new App();
const parameters = process.argv.slice(2);

(async function () {
    await app.analyze(parameters[0])
})()
